import '@babel/polyfill'
import Vue from 'vue'
import App from './App'
import 'jquery'
import 'bootstrap'

//event bus
Vue.prototype.$bus = new Vue();

new Vue({
    el: '#app',
    template: '<App/>',
    components: {
        App
    },
})